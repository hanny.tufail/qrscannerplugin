library twitter_qr_scanner;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;
import 'dart:math';
import 'dart:typed_data';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toast/toast.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';

typedef void QRViewCreatedCallback(QRViewController controller);

class QRView extends StatefulWidget {
  const QRView({
    @required Key key,
    @required this.onQRViewCreated,
    @required this.data,
    this.overlay,
    this.qrCodeBackgroundColor = Colors.blue,
    this.qrCodeForegroundColor = Colors.white,
    this.switchButtonColor = Colors.white,
  })  : assert(key != null),
        assert(onQRViewCreated != null),
        assert(data != null),
        super(key: key);

  final QRViewCreatedCallback onQRViewCreated;

  final ShapeBorder overlay;
  final String data;
  final Color qrCodeBackgroundColor;
  final Color qrCodeForegroundColor;
  final Color switchButtonColor;

  @override
  State<StatefulWidget> createState() => _QRViewState();
}

class _QRViewState extends State<QRView> {
  bool isScanMode = true;
  CarouselSlider slider;
  var flareAnimation = "view";
  File result;

  GlobalKey _globalKey = new GlobalKey();

  Future<void> saveQrImage() async {
    try {
      print('inside');
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      var bs64 = base64Encode(pngBytes);
      Map<Permission, PermissionStatus> statuses = await [
        Permission.mediaLibrary,
        Permission.storage,
        Permission.photos,
      ].request();
      if (statuses[Permission.mediaLibrary].isGranted &
              statuses[Permission.storage].isGranted &&
          statuses[Permission.photos].isGranted) {
        final result = await ImageGallerySaver.saveImage(pngBytes);
        Toast.show("QR Image Saved Successfully !", context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        setState(() {});
      }
      Navigator.of(context).pop();
    } catch (e) {
      print(e);
    }
  }

  Future<void> shareQrImage() async {
    try {
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      try {
        await Share.files(
          'esys images',
          {
            'qrimage.png': pngBytes,
          },
          'image/png',
        );
        setState(() {});
      } catch (e) {
        print('Error easy share plugin : $e');
      }
    } catch (e) {
      print('Exception of capturing image is : $e');
    }
    Navigator.of(context).pop();
  }

  void shareQr(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) {
          return Container(
            child: Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Stack(
                alignment: Alignment.centerLeft,
                children: <Widget>[
                  Container(
                    width: 100.0,
                    height: 80.0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            child: Text(
                              'Save',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 12.0,
                              ),
                            ),
                            onPressed: saveQrImage,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            onPressed: shareQrImage,
                            child: Text(
                              'Share via',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 12.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> getPhotoByGallery() async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera);
    Uint8List bytes = file.readAsBytesSync();
//    String barcode = await scanner.scanBytes(bytes);
//    print('Data of the file : $barcode');
  }

//  Future decode(File image) async {
//    //print(path);
//    try {
//      String data = await FlutterQrReader.imgScan(image);
//      setState(() {
//        _data = data;
//      });
//      print('Data is $data');
//    } catch (e) {
//      print('Exception thown is $e');
//    }
//  }

  getSlider() {
    setState(() {
      slider = CarouselSlider(
        height: MediaQuery.of(context).size.height,
        viewportFraction: 1.0,
        enableInfiniteScroll: false,
        onPageChanged: (index) {
          setState(() {
            isScanMode = index == 0;
            if (isScanMode) {
              flareAnimation = "viewToScan"; //
            } else {
              flareAnimation = "scanToView";
            }
          });
        },
        items: [
          Container(
            alignment: Alignment.center,
            decoration: ShapeDecoration(
              shape: widget.overlay,
            ),
            child: RepaintBoundary(
              key: _globalKey,
              child: Container(
                width: 220,
                height: 220,
                padding: EdgeInsets.all(21),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: widget.qrCodeBackgroundColor,
                ),
                child: QrImage(
                  data: widget.data,
                  version: QrVersions.auto,
                  foregroundColor: widget.qrCodeForegroundColor,
                  gapless: true,
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            decoration: ShapeDecoration(
              shape: widget.overlay,
            ),
            child: Container(
              width: 220,
              height: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.transparent,
              ),
              child: result != null
                  ? Container(
                      width: double.infinity,
                      height: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16.0)),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            top: 3.0,
                            left: 4.5,
                            right: 4.5,
                            child: Container(
                              width: 190.0,
                              height: 70.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: Colors.blue,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 47.0,
                            left: 12.0,
                            child: CircleAvatar(
                              radius: 32.0,
                              backgroundColor: Colors.white,
                              child: ClipOval(
                                child: Image.file(
                                  result,
                                  width: 55.0,
                                  height: 55.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 80.0,
                            right: 10.0,
                            child: GestureDetector(
                              onTap: () {},
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  border: Border.all(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),
                                child: Text(
                                  'Save to Wallet',
                                  style: TextStyle(fontSize: 10.0),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 120,
                            left: 20.0,
                            child: Text(
                              'Name',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Positioned(
                            top: 138,
                            left: 20.0,
                            child: Text(
                              'Designation',
                              style: TextStyle(
                                fontSize: 15.0,
                                color: Colors.grey[500],
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 20.0,
                            left: 15.0,
                            child: Text(
                              'Company Name',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue[300],
                              ),
                            ),
                          ),
                          Positioned(
                              bottom: 15.0,
                              right: 10.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Image.file(
                                  result,
                                  height: 40.0,
                                  width: 40.0,
                                ),
                              )
//                            CircleAvatar(
//                              radius: 20.0,
//                              backgroundColor: Colors.white,
//                              child: ClipOval(
//                                child: Image.file(
//                                  result,
//                                  width: 55.0,
//                                  height: 55.0,
//                                  fit: BoxFit.cover,
//                                ),
//                              ),
//                            ),
                              ),
                        ],
                      ),
                    )
                  : Container(),
            ),
          ),
        ],
      );
    });
    return slider;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _getPlatformQrView(),
        widget.overlay != null ? getSlider() : Container(),
        Align(
          alignment: Alignment.topLeft,
          child: SafeArea(
              child: IconButton(
            icon: Icon(
              Icons.clear,
              color: Colors.white70,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )),
        ),
        Align(
          alignment: Alignment.topRight,
          child: SafeArea(
            child: isScanMode == false
                ? IconButton(
                    icon: Icon(
                      Icons.photo,
                      color: Colors.white70,
                    ),
                    onPressed: () async {
                      var image = await ImagePicker.pickImage(
                          source: ImageSource.gallery);
                      if (image != null) {
//                        final result = await RScan.scanImagePath(image.path);
//                        setState(() {
//                          this.result = result;
                        setState(() {
                          result = image;
                        });
                        print('Image picked form gallery !');
//                        });
                      }
                    },
                  )
                : IconButton(
                    icon: Icon(
                      Icons.share,
                      color: Colors.white,
                    ),
                    onPressed: () => shareQr(context),
                  ),
          ),
        ),
        Positioned(
          bottom: 16,
          left: 0,
          right: 0,
          child: InkWell(
            onTap: () {
              setState(() {
                isScanMode = !isScanMode;
                if (isScanMode) {
                  flareAnimation = "scanToView";
                  slider?.previousPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.linear);
                } else {
                  flareAnimation = "viewToScan";

                  slider?.nextPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.linear);
                }
              });
            },
            child: Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(255),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(255),
                child: FlareActor(
                  "packages/twitter_qr_scanner/asset/QRButton.flr",
                  alignment: Alignment.center,
                  animation: flareAnimation,
                  fit: BoxFit.contain,
                  color: widget.switchButtonColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getPlatformQrView() {
    Widget _platformQrView;
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        _platformQrView = AndroidView(
          viewType: 'com.anka.twitter_qr_scanner/qrview',
          onPlatformViewCreated: _onPlatformViewCreated,
        );
        break;
      case TargetPlatform.iOS:
        _platformQrView = UiKitView(
          viewType: 'com.anka.twitter_qr_scanner/qrview',
          onPlatformViewCreated: _onPlatformViewCreated,
          creationParams: _CreationParams.fromWidget(0, 0).toMap(),
          creationParamsCodec: StandardMessageCodec(),
        );
        break;
      default:
        throw UnsupportedError(
            "Trying to use the default webview implementation for $defaultTargetPlatform but there isn't a default one");
    }
    return _platformQrView;
  }

  void _onPlatformViewCreated(int id) async {
    if (widget.onQRViewCreated == null) {
      return;
    }
    widget.onQRViewCreated(QRViewController._(id, widget.key));
  }
}

class _CreationParams {
  _CreationParams({this.width, this.height});

  static _CreationParams fromWidget(double width, double height) {
    return _CreationParams(
      width: width,
      height: height,
    );
  }

  final double width;
  final double height;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'width': width,
      'height': height,
    };
  }
}

class QRViewController {
  static const scanMethodCall = "onRecognizeQR";

  final MethodChannel _channel;

  StreamController<String> _scanUpdateController = StreamController<String>();

  Stream<String> get scannedDataStream => _scanUpdateController.stream;

  QRViewController._(int id, GlobalKey qrKey)
      : _channel = MethodChannel('com.anka.twitter_qr_scanner/qrview_$id') {
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      final RenderBox renderBox = qrKey.currentContext.findRenderObject();
      _channel.invokeMethod("setDimensions",
          {"width": renderBox.size.width, "height": renderBox.size.height});
    }
    _channel.setMethodCallHandler(
      (MethodCall call) async {
        switch (call.method) {
          case scanMethodCall:
            if (call.arguments != null) {
              _scanUpdateController.sink.add(call.arguments.toString());
            }
        }
      },
    );
  }

  void flipCamera() {
    _channel.invokeMethod("flipCamera");
  }

  void toggleFlash() {
    _channel.invokeMethod("toggleFlash");
  }

  void pauseCamera() {
    _channel.invokeMethod("pauseCamera");
  }

  void resumeCamera() {
    _channel.invokeMethod("resumeCamera");
  }

  void dispose() {
    _scanUpdateController.close();
  }
}

//_data != null
//? Container(
//width: 240.0,
//height: 240.0,
//padding: EdgeInsets.all(22),
//decoration: BoxDecoration(
//borderRadius: BorderRadius.circular(16),
//),
//child: Container(
//child: Stack(
//children: <Widget>[
//Positioned(
//top: 1.0,
//bottom: -1.0,
//child: Container(
//width: 220.0,
//height: 80.0,
//decoration: BoxDecoration(
//borderRadius: BorderRadius.only(
//topLeft: Radius.circular(15.0),
//topRight: Radius.circular(15.0),
//),
//color: Colors.blue,
//),
//),
//),
//Row(
//mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//children: <Widget>[
//Positioned(
//top: 20.0,
//left: 10.0,
//child: CircleAvatar(
//backgroundColor: Colors.grey,
//radius: 20.0,
//),
//),
//Positioned(
//top: 20.0,
//left: 100.0,
//child: FlatButton(
//color: Colors.white,
//textColor: Colors.blue,
//disabledColor: Colors.grey,
//disabledTextColor: Colors.black,
//padding: EdgeInsets.all(8.0),
//splashColor: Colors.blueAccent,
//onPressed: () {
///*...*/
//},
//child: Text(
//"Save to Wallet",
//style: TextStyle(fontSize: 20.0),
//),
//),
//),
//],
//),
//Positioned(
//top: 30.0,
//left: 10.0,
//child: Text(
//'Personal Name',
//style: TextStyle(
//fontSize: 15.0,
//),
//),
//),
//Positioned(
//top: -1.0,
//left: 10.0,
//child: Text(
//'Company Name',
//style: TextStyle(
//fontSize: 15.0,
//),
//),
//),
//],
//),
//),
//)
